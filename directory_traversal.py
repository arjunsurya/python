#Import the os module, for the os.walk function
import os
 
# Read the directory you want to start from
rootDir = input('Enter the directory to start from: ')
for dirname, subdirlist, filelist in os.walk(rootDir):
    print('Found directory: %s' % dirname)
    for sdname in subdirlist:
        print('\t%s' %sdname)
    for fname in filelist:
        print('\t\t%s' % fname)
    print('******************************')

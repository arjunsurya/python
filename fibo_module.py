def fib(n=5):
    """
    Generates Fibonacci series up to specified number.
   
    Can run as script also. Has proper validations defined.
    """
    if(n <0): raise ValueError("Please enter positive number !!!!")
    res = []
    a, b = 0, 1
    while b < n:
        print b,
        res.append(b)
        a, b = b, a + b
    return res

if __name__ == "__main__":
    import sys
    fib(int(sys.argv[1]))

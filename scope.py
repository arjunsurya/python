
v = 100

def foo():
    global v
    print "In foo: v =", v
    v = 200
    print "In foo: v changed to", v

print "In main: v =", v
foo()
print "In main: v now is", v



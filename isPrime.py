def is_prime(x):
    from math import sqrt
    r = int(sqrt(x))
    for i in range(2, r +1):
        if x % i == 0 : return False
    return True  

num = input("Enter a number: ")
if is_prime(num):
    print num, "is a prime number"
else:
    print num, "is NOT a prime number"

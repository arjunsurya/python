def is_prime(x):
    from math import sqrt
    for i in range(2, int(sqrt(x))+1):
        if x % i == 0: return False
    return True

def prime_factors(x):
    for i in range(2, (x/2)+1):
        if (x % i == 0) and is_prime(i): yield i

num = input("Enter a number: ")
print "Prime factors (unique) are: ",

for i in prime_factors(num): print i,




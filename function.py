def add_record(name, age, city='Bengaluru'):
    print "Name:", name, ", Age:", age, ", City:", city  



data = [ ["john", 10, "Delhi"],
         ["sam", 11, "Mumbai"],
         ["smith", 20, "Bengaluru"],
         ["joe", 17, "Chennai"]
]

for rec in data:
#    add_record(rec[0], rec[1], rec[2])
    add_record(*rec)

